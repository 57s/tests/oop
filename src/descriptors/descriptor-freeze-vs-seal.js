// freeze - свойства нельзя добавить удалить или изменить

const objFreeze = { prop: 1 };

Object.freeze(objFreeze);

objFreeze.prop = 2; // значение не изменится
objFreeze.newProp = 3; // свойство не будет добавлено
delete objFreeze.prop; //  свойство не будет удалено

console.log(objFreeze); // { prop: 1 }
console.log(Object.isFrozen(objFreeze)); // true

// seal - свойства нельзя добавить удалить но можно ИЗМЕНИТЬ

const objSeal = { prop: 1 };
Object.seal(objSeal);

objSeal.prop = 2; // Значение изменится
objSeal.newProp = 3; // свойство не будет добавлено
delete objSeal.prop; //  свойство не будет удалено

console.log(objSeal); // { prop: 2 }
console.log(Object.isSealed(objSeal)); // true

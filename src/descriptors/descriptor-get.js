const obj = {
  prop: 42,
};

// Получаем дескриптор свойства 'prop'
const descriptor = Object.getOwnPropertyDescriptor(obj, 'prop');

console.log(descriptor);

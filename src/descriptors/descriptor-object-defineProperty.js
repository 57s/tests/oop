const obj = {};

// Определяем новое свойство с дескриптором
Object.defineProperty(obj, 'property', {
  value: 42,
  writable: false, // нельзя изменять значение свойства
  enumerable: true, // свойство будет перечисляться в циклах
  configurable: true, // можно удалять свойство
});

console.log(obj.property); // 42

// Попробуем изменить значение свойства
obj.property = 100;
console.log(obj.property); // 42

const myPrototype = {
  name: 'Я будущий прототип',
  sayHello: () => {
    console.log('Привет');
  },
};

// Создание объекта с помощью метода "create" глобального объекта "Object"
// create принимает два параметра - объекта
// 1) прототип для создаваемого объекта
// 2) объект с ключами создаваемого объекта
const object2 = Object.create(myPrototype, {
  // Настройки свойства "name" => "PropertyDescriptorMap"
  name: {
    // Значение поля "name" = "Петрович"
    value: 'Петрович',
    // Разрешить массивам перебрать свойство
    enumerable: true,
    // Разрешить перезаписывать значение свойства
    writable: true,
    // Разрешает удалять ключ из объекта
    configurable: true,
  },
  // Настройки свойства "age"
  age: {
    value: 100,
    // По умолчанию свойства "enumerable", "writable", "configurable"
    // имеют значение "false"
    enumerable: false,
    writable: false,
    configurable: false,
  },
  test: {
    // Геттер(getter) это функция, которая возвращает значение свойства или поля в объекте
    // Отработает при попытке получить значение свойства "test"
    get() {
      document.body.style.background = 'violet';
      // Подразумевается что он что то вернет
      return 'I am getter';
    },
    // Сеттер(setter)- это функция, которая используется для обновления свойств объекта
    // Отработает при попытке изменить значение свойства "test"
    set(value) {
      document.body.style.background = 'gold';
      console.log(value, 'I am setter');
    },
  },
});

for (let key in object2) {
  // Не показывать унаследуемте свойства (hasOwnProperty)
  // Так же не будет `age`, `test` потому что `enumerable` = `false`
  if (object2.hasOwnProperty(key)) {
    console.log('KEY: ', key, 'VALUE:', object2[key]);
  }
}

// console.log(object2.test);
// object2.sayHello();

// console.log(object2);

const usersArr = [
  { id: 1, name: 'Alice', age: 30, email: 'alice@example.com' },
  { id: 2, name: 'Bob', age: 25, email: 'bob@example.com' },
  { id: 3, name: 'Charlie', age: 35, email: 'charlie@example.com' },
  { id: 4, name: 'David', age: 28, email: 'david@example.com' },
  { id: 5, name: 'Eve', age: 22, email: 'eve@example.com' },
  { id: 6, name: 'Frank', age: 33, email: 'frank@example.com' },
  { id: 7, name: 'Grace', age: 27, email: 'grace@example.com' },
  { id: 8, name: 'Hank', age: 31, email: 'hank@example.com' },
  { id: 9, name: 'Ivy', age: 29, email: 'ivy@example.com' },
  { id: 10, name: 'Jack', age: 24, email: 'jack@example.com' },
  { id: 21, name: 'Karen', age: 26, email: 'karen@example.com' },
  { id: 32, name: 'Leo', age: 32, email: 'leo@example.com' },
  { id: 43, name: 'Mia', age: 23, email: 'mia@example.com' },
  { id: 54, name: 'Nina', age: 34, email: 'nina@example.com' },
  { id: 65, name: 'Oscar', age: 21, email: 'oscar@example.com' },
  { id: 76, name: 'Paul', age: 36, email: 'paul@example.com' },
  { id: 87, name: 'Quinn', age: 27, email: 'quinn@example.com' },
  { id: 98, name: 'Ruby', age: 25, email: 'ruby@example.com' },
];

const handler = {
  construct(target, [args]) {
    const indexesMap = {};

    args.forEach((item) => {
      indexesMap[item.id] = item;
    });

    const handlers = {
      get(arr, propName) {
        switch (propName) {
          case 'push': {
            return (item) => {
              indexesMap[item.id] = item;
              arr[propName].call(arr, item);
            };
          }
          case 'findById': {
            return (id) => indexesMap[id];
          }
          default: {
            return arr[propName];
          }
        }
      },
    };

    return new Proxy(new target(...args), handlers);
  },
};

const IndexedArray = new Proxy(Array, handler);

const users = new IndexedArray(usersArr);

const newUSer = {
  id: 100,
  name: 'Alex',
  age: 88,
  email: 'test@example.com',
};

users.push(newUSer);
console.log(users.findById(100));

function withHiddenProps({ data, hiddenProps = [], prefix = '_' }) {
  const checkHidden = (propName) =>
    hiddenProps.includes(propName) || propName.startsWith(prefix);

  return new Proxy(data, {
    get(target, propName, receiver) {
      return propName in receiver ? target[propName] : undefined;
    },

    has(target, propName) {
      if (checkHidden(propName)) {
        return false;
      }

      return propName in target;
    },

    ownKeys(target) {
      return Reflect.ownKeys(target).filter((key) => !checkHidden(key));
    },
  });
}

const data = {
  name: 'Alex',
  _secret: 'TOP SECRET',
  password: '12345',
};

const hiddenData = withHiddenProps({ data, hiddenProps: ['password'] });

console.log(hiddenData._secret);
console.log(hiddenData.password);
console.log(hiddenData.name);

console.log('password' in hiddenData);
console.log('_secret' in hiddenData);

for (const key in hiddenData) {
  console.log(hiddenData[key]);
}

console.log(Object.keys(hiddenData));

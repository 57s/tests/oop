const basket = {
  items: [
    { name: 'Яблоко', price: 30, count: 5 },
    { name: 'Груша', price: 50, count: 7 },
    { name: 'Банан', price: 70, count: 3 },
  ],
  discount: 0.3,
  total: 0,
};

const handler = {
  get(target, propName) {
    if (propName === 'total') {
      return Math.ceil(
        target.items.reduce((acc, item) => acc + item.price * item.count, 0) /
          target.discount
      );
    }

    return target[propName];
  },

  set(target, propName, newValue) {
    switch (propName) {
      case 'discount':
      case 'total': {
        throw new Error(`Нельзя изменить свойство ${propName}`);
      }
    }

    target[propName] = newValue;
    return true;
  },
};

const basketProxy = new Proxy(basket, handler);

try {
  basketProxy.discount = 0.5; // Попытка изменения свойства "discount" выбросит ошибку
} catch (e) {
  console.error(e.message); // "Нельзя изменить свойство discount"
} finally {
  console.log(basketProxy.total);
}

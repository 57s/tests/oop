function createReactiveState({ state, updateCallback }) {
  const handlers = {
    set(target, prop, value, receiver) {
      updateCallback({ prop, value });
      return Reflect.set(target, prop, value, receiver);
    },
  };

  return new Proxy(state, handlers);
}

function updateUI({ prop, value }) {
  console.log(
    `Обновление пользовательского интерфейса: свойство "${prop}" изменено на "${value}"`
  );
}

const initState = {
  name: 'Alice',
  age: 25,
};

const user = createReactiveState({
  state: initState,
  updateCallback: updateUI,
});

user.age = 30;

console.log(user);

const target = {};

const handler = {
  get(target, propName) {
    console.log(`Перехват попытки получить свойство "${propName}"`);

    return target[propName];
  },

  set(target, propName, newValue) {
    console.log(
      `Перехват попытки изменить свойство "${propName}" на значение "${newValue}"`
    );
    target[propName] = newValue;
  },

  deleteProperty(target, propName) {
    console.log(`Перехват попытки удалить свойство "${propName}"`);
    delete target[propName];
    return true; // Необходимо вернуть true для подтверждения успешного удаления
  },

  has(target, propName) {
    console.log(`Перехват попытки проверить наличие свойства "${propName}"`);
    return propName in target;
  },
};

const objectProxy = new Proxy(target, handler);

objectProxy.name = 'Alex';
delete objectProxy.name;

console.log('name' in objectProxy);
console.log(objectProxy.name);

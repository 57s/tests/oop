class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
}

const handler = {
  construct: function (target, args) {
    console.log(`Создание нового экземпляра(${target.name}) с аргументами: ${args}`);
    return new target(...args);
  },
};

const PersonProxy = new Proxy(Person, handler);

const person = new PersonProxy('John', 30);

console.log(person);
console.log(person.name); // "John"
console.log(person.age); // 30

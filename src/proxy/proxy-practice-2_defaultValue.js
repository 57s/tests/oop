function withDefaultValue(object, defaultValue = 0) {
  return new Proxy(object, {
    get: (target, propName) => {
      return propName in target ? target[propName] : defaultValue;
    },
  });
}

const position = withDefaultValue({ x: 24, y: 42 }, 0);

console.log(position);
console.log(position.x);
console.log(position.z);

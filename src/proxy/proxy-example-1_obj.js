const target = {
  message: 'Hello, world!',
};

const handler = {
  get: function (target, propName, receiver) {
    // target - Объект за которым мы следим
    // propName - текущее получаемое свойство
    // receiver - Объект-прокси через который была вызвана операция

    if (propName in target) {
      // Если в объекте есть такое свойство то вернем его значение
      return target[propName];
    } else {
      // Если такого свойства нет, скажем об этом
      return `Свойства "${propName}" в объекте нет`;
    }
  },
};

const objectProxy = new Proxy(target, handler);

console.log(objectProxy.message);
console.log(objectProxy.name);

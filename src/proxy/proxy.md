`Proxy` — это гибкий объект, который позволяет перехватывать и настраивать базовые операции,
такие как чтение, запись, вызов функций и другие.
С помощью `Proxy` вы можете создавать ловушки `traps` для этих операций и изменять их поведение по своему усмотрению.

const log = (data) => console.log(`LOG: ${data}`);

const handler = {
  count: 0,
  apply(target, thisArg, argumentsList) {
    // target - функция за которой следим
    // thisArg - контекст(this) этой функции
    // argumentsList = массив аргументов переданных в это функцию

    // Мы можем например считать количество вызовов этой функции
    ++this.count;

    console.log(
      'Перехватив функцию мы получаем доступ к: ',
      {
        target,
        thisArg,
        argumentsList,
      },
      `Количество вызовов функции(${target.name} = ${this.count})`
    );

    // Если мы перехватываем вызов функции то нужно вернуть результат которая должна была выдавать перехваченная функция
    return target.apply(thisArg, argumentsList);
  },
};

const funcProxy = new Proxy(log, handler);

funcProxy('test');
funcProxy('test');

class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
}

const handler = {
  construct: function (target, args) {
    console.log(
      `Создание нового экземпляра(${target.name}) с аргументами: ${args}`
    );
    return new Proxy(new target(...args), {
      get(t, propName) {
        console.log(`Getting prop "${propName}"`);
        return t[propName];
      },
    });
  },
};

const PersonProxy = new Proxy(Person, handler);

const person = new PersonProxy('John', 30); // "Creating a new instance with arguments: John,30"
console.log(person);
console.log(person.name); // "John"
console.log(person.age); // 30

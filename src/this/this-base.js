const person = {
  name: 'Alice',
  greet() {
    console.log(`Hello, my name is ${this.name}`);
  },
};

const { greet } = person;

setTimeout(greet, 100); // Потеря контекста, this будет undefined


const numbers = {
  numberA: 5,
  numberB: 10,
  sum: function () {
    function calculate() {
      return this.numberA + this.numberB;
    }
    return calculate();
  },
};
console.log(numbers.sum()); // Потеря контекста, будет NaN


function greeting() {
  console.log(`Hello, my name is ${this.name}.`);
}


// greeting.call(person); // "Hello, my name is Alice."
// greeting.apply(person); // "Hello, my name is Alice."
// const boundGreet = greeting.bind(person);
// boundGreet(); // "Hello, my name is Alice."

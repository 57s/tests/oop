# This
this` — это ключевое слово, которое ссылается на контекст исполнения функции.
Значение `this` может изменяться в зависимости от контекста, в котором была вызвана функция.

__Особенности:__

- В браузере: глобальный объект — это `window`.
- В Node.js: глобальный объект — это `global`.
- В строгом режиме (`"use strict"`): `this` имеет значение `undefined` и в браузере и в node.
- Когда функция вызывается как метод объекта, `this` ссылается на этот объект.
- В классах и конструкторах (функциях, вызываемых с ключевым словом `new`) `this` ссылается на вновь созданный объект.
- Стрелочные функции не имеют собственного контекста `this`. Они захватывают значение `this` из внешней функции в момент их создания.
- Методы `call`, `apply` и `bind` позволяют явно задавать значение `this`
- Контекст `this` можно потерять, например, при передаче метода объекта в качестве колбэка или обработчика события.


Можно явно задать значение `this` с помощью методов `call`, `apply` и `bind`.

- `call`: вызывает функцию с указанным значением this и аргументами.
- `apply`: вызывает функцию с указанным значением this и массивом аргументов.
- `bind`: возвращает новую функцию с указанным значением this.

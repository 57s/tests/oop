# Object

В js все объект

## Create

- Литерал объекта
- Конструктор new Object();
- Функция-конструктор
- Object.create()
- Классы (ES6)
- Фабричная функция
- Копирование с помощью Object.assign()
- Оператор spread

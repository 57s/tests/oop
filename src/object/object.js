// Литерал объекта (Синтаксический сахар)
const wrapObject1 = {
  name: 'Иннокентий',
  age: '77',
};

// new Object
const object1 = new Object({ name: 'Иннокентий', age: '77' });

// функция конструктор
function Person(name, age) {
  this.name = name;
  this.age = age;
}

const object2 = new Person('Иннокентий', '77');

// Object.create
const proto = { greeting: 'Hello' };
const object3 = Object.create(proto);

object3.name = 'Иннокентий';
object3.age = '77';

// Class
class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
}

const object4 = new Person('Иннокентий', '77');

// Фабричная функция

function createPerson(name, age) {
  return {
    name: name,
    age: age,
  };
}
const object5 = createPerson('Иннокентий', '77');

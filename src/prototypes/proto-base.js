// Все наследуется от объекта

const myObject = {
	name: 'my object',
	desc: 'Я Object',
};

// Расширения методов объекта 'Object'
// Добавление нового метода 'test'
// Этот метод будет наследоваться всеми объектами

Object.prototype.test = () => {
	console.log('Я новый кастомный метод, который наследуется всеми объектами');
};

console.log('TEST №1================>');

myObject.test();
console.log(myObject);

// Если функция это объект
const fun = () => {
	console.log('fhdh');
};

// То получается у функции "fun"
// можно вызвать кастомный метод "test" объекта "Object"
// который унаследовала функция "test"
fun.test();

console.log('TEST №2================>');

// Создать объект "dog" прототипом которого будет "myObject"
const dog = Object.create(myObject);

// Объект пустой
console.log(dog);
// Но он унаследовал объект "myObject"
console.log(dog.name);

console.log('TEST №3================>');

const string = 'О боже я тоже ОБЪЕКТ';

console.log('Стока, которая объект: ', string);
string.test();

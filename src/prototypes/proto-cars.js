const protoCars = {
  drive() {
    this.gas -= 10;
    console.log(`Остаток бензина ${this.gas}`);
  },
};

function CreateCar(name, price, gas) {
  this.name = name;
  this.price = price;
  this.gas = gas;

  this.engine = function () {
    console.log('work...work...work');
  };
}

CreateCar.prototype = protoCars;

const bmw = new CreateCar('BMW', '5 000', 100);
const tesla = new CreateCar('Tesla', '3 000', 100);

bmw.drive();
bmw.drive();
bmw.drive();

console.log(bmw);
console.log(tesla);

tesla.engine();

console.log(bmw.drive === tesla.drive); // true потому что у них общий прототип
console.log(bmw.engine === tesla.engine); // false потому что разные экземпляры метода engine

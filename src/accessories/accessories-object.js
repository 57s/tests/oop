const person = {
  _name: 'Alice',

  get name() {
    return this._name;
  },

  set name(newName) {
    if (typeof newName === 'string' && newName.trim() !== '') {
      this._name = newName;
    } else {
      console.log('Invalid name');
    }
  },
};

console.log(person.name); // "Alice"
person.name = 'Bob';
console.log(person.name); // "Bob"
person.name = ''; // "Invalid name"
console.log(person.name); // "Bob"

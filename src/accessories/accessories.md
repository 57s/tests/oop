# Аccessors

`accessors` - это специальные методы, которые используются для получения (getter) и задания (setter) значений свойств объекта.

`Getters` позволяют определить метод для чтения значения свойства.

```js
const obj = {
  _name: 'Alice',

  get name() {
    return this._name;
  },
};

console.log(obj.name); // "Alice"
```

`Setters` позволяют определить метод для изменения значения свойства.

```js
const obj = {
  _name: 'Alice',

  get name() {
    return this._name;
  },

  set name(newName) {
    this._name = newName;
  },
};

obj.name = 'Bob';
console.log(obj.name); // "Bob"
```

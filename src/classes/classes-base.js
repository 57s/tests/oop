// В JS классы являются синтаксическим сахаром, который основан на прототипном наследовании
// Классы по сути это гибкий инструмент для управления объектами

// Раньше в js для этих целий использовались функции конструкторы
function Constructor(name, age) {
	this.name = name;
	this.age = age;
}

const constructorTest = new Constructor('Уж', 50);

// console.log(constructorTest);

// Пример №1 ==================>

// Создать класс "User"
class User {
	// конструктор объекта
	// получает два аргумента имя("name") и возраст("age")
	// и на основе их конструирует объект
	constructor(name, age) {
		this.name = name;
		this.age = age;
	}
}

// Создать класс "Admin" который будет наследоваться от класса "User"
class Admin extends User {
	// Расширение конструируемого объекта
	// добавляется третий аргумент "type", тип пользователя
	constructor(name, age, type) {
		// Вызвать конструктор родительского класса
		// что бы получить доступ к "name" "type"
		super(name, age);
		this.type = type;
	}
}

const roman = new Admin('Роман', 20, 'mentor');
// console.log(roman);

// Пример №2 ==================>

class Animal {
	// Статическое свойство, которое доступно только при обращении к "Animal" и внутри его
	static type = 'Animal';

	// получает объект "options"
	// и на основе свойств объекта конструирует объект
	constructor(options) {
		this.name = options.name;
		this.age = options.age;
	}

	// метод "voice" класса "Animal"
	voice() {
		console.log('I am Animal');
	}
}

class Cat extends Animal {
	constructor(options) {
		super(options);
		this.color = options.color;
	}
	// Переопределить метод "voice" супер(родительского) класса "Animal" для под класса(дочернего) "Cat"
	voice() {
		// Вызвать метод "voice" родительского класса "Animal"
		super.voice();
		console.log('I am cat');
	}

	// создать геттер для класса "Cat" который переводит кошачий возраст в человечий
	get ageInfo() {
		return this.age * 7;
	}

	// создать сеттер для класса "Cat" который задает новый возраст коту
	set ageInfo(newAge) {
		this.age = newAge;
	}
}

const cat = new Cat({ name: 'Барсик', age: 20, color: 'black' });

// const cat = new Animal({ name: 'Барсик', age: 20 });

// console.log(cat);
// cat.voice();

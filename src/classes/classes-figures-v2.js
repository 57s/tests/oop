// Tools
const $ = document.querySelector.bind(document);

const Dom = class {
  constructor() {}

  create({ dom = 'div', text, classes, id }) {
    let element = document.createElement(dom);

    if (text !== undefined) {
      element.textContent = text;
    }

    if (id !== undefined) {
      element.id = id;
    }

    if (Array.isArray(classes)) {
      classes.forEach((clas) => element.classList.add(clas));
    } else if (classes !== undefined) {
      element.classList.add(classes);
    }

    return element;
  }

  add(element, parent = $('body')) {
    parent.appendChild(element);
  }

  show(selector = this.id) {
    const element = $(selector);

    element.style.display = 'block';

    return element;
  }

  hide(selector = this.id) {
    const element = $(selector);

    element.style.display = 'none';

    return element;
  }

  remove(selector = this.id) {
    const element = $(selector);
    element.remove();
  }
};

const Component = class extends Dom {
  constructor(id, style, parent) {
    super();
    this.id = id;
    this.$parent = $(parent);
    this.$component = super.create({ id: id });
    this.componentStyles = style;

    this.addStyles(style);
  }

  addStyles(styles = this.componentStyles, element = this.$component) {
    Object.entries(styles).forEach(
      ([key, value]) => (element.style[key] = value)
    );
  }

  add(element = this.$component, parent = $('body')) {
    parent.appendChild(element);
  }

  hide() {
    this.$component.style.display = 'none';
  }

  show() {
    this.$component.style.display = 'block';
  }

  remove() {
    this.$component.remove();
  }
};

// const Box = class extends Component {};
// const Card = class extends Component {};
// const Modal = class extends Component {};
const Square = class extends Component {
  constructor(id, style) {
    super(id, style);
  }
};

const Circle = class extends Component {
  constructor(id, style) {
    super(id, style);
    this.$component.style.borderRadius = '50%';
  }
};

const square = new Square('square1', {
  backgroundColor: 'purple',
  color: 'brown',
  width: '100px',
  height: '100px',
});

const circle = new Circle('circle1', {
  backgroundColor: 'gold',
  color: 'brown',
  width: '150px',
  height: '150px',
});

square.add();
circle.add();
// circle.hide();

// square.remove();
// square.hide();
// square.show();

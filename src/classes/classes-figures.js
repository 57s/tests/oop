// Пример №3 ==================>
class Component {
	constructor(selector = 'body', id = 'test') {
		// через доллар указывают переменные которые содержат dom node
		this.$parent = document.querySelector(selector);
		this.id = id;
		this.$elem = this.create('div', id);
	}

	show() {
		this.$elem.style.display = 'block';
	}

	hide() {
		this.$elem.style.display = 'none';
	}

	create(dom = 'div', id = this.id) {
		let element = document.createElement(dom);
		element.id = id;
		return element;
	}

	draw(id = this.$elem) {
		this.$parent.appendChild(id);
	}

	delete() {
		this.$elem.remove();
	}
}

class Box extends Component {
	constructor(options) {
		super(options.selector, options.id);
		this.$elem.style.width = this.$elem.style.height = options.size + 'px';
		this.$elem.style.background = options.color;
	}
}

class Circle extends Box {
	constructor(options) {
		super(options);
		this.$elem.style.borderRadius = '50%';
	}
}

const square = new Box({
	selector: '.page',
	id: 'square',
	size: 100,
	color: 'red',
});

const square2 = new Box({
	selector: '.page',
	id: 'square2',
	size: 150,
	color: 'gold',
});

const circle = new Circle({
	selector: '.page',
	id: 'circle',
	size: 200,
	color: 'purple',
});

square.draw();
square2.draw();
circle.draw();

square.hide();
square.show();
// square.delete();

// не работает
circle.create();

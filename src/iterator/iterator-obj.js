function toIterable(obj) {
  return {
    ...obj,
    [Symbol.iterator]() {
      const keys = Object.keys(this);
      const limit = keys.length;
      const self = this;

      let counter = 0;

      return {
        next() {
          if (counter < limit) {
            return {
              done: false,
              value: self[keys[counter++]],
            };
          }
          return {
            done: true,
          };
        },
      };
    },
  };
}

const person = {
  name: 'Alex',
  age: 25,
  city: 'Odessa',
};



for (let value of toIterable(person)) {
  console.log(value);
}


console.log([...toIterable(person)]);
